from machine import UART
from micropython import const
import ujson
import os
import logging


PREAMBLE = b'\x12\x34\x56'
_MAX_BODY_SIZE = const(2 ** 16 - 1)  # uint16


port = None
handlers = {}


def send(data):
    body = ujson.dumps(data).encode('utf-8')
    body_size = len(body)
    if body_size > _MAX_BODY_SIZE:
        logging.error('Message size exceeded')
        return 0
    message = PREAMBLE + body_size.to_bytes(2, 'little') + body
    sent_bytes = port.write(message)
    logging.debug(f'Message sent [{sent_bytes=}]')
    return sent_bytes


def recv():
    for i in PREAMBLE:
        if port.read(1)[0] != i:
            logging.warn('Packets not aligned')
            return {}
    body_size = int.from_bytes(port.read(2), 'little')
    body = port.read(body_size)
    return ujson.loads(body)


def init():
    global port
    port = UART(0, 115200, timeout=200)
    os.dupterm(None, 1)  # disable REPL
    logging.debug('REPL disabled')
    logging.info('UART initialized')


def handle_msg():
    if port.any():
        msg = recv()
        try:
            cmd = msg['cmd']
        except KeyError:
            logging.warn('Received message has cmd field missing')
            return
        logging.debug(f'Received message [{cmd=}]')
        try:
            handlers[cmd](msg)
        except KeyError:
            logging.warn(f'Unknown command [{cmd=}]')


def register_handler(cmd):
    def register(func):
        if cmd not in handlers:
            handlers[cmd] = func
            logging.debug(f'Handler function registered [{cmd=}, {func=}]')
        else:
            logging.error(f'Cannot register handler, cmd already taken [{cmd=}, {func=}]')
        return func
    return register
