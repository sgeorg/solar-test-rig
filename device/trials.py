import ujson
import os
import logging


_TRIAL_DIR = 'trial'


def _create_id():
    current_ids = sorted(Trial.list(), reverse=True)
    if not current_ids:
        return int.from_bytes(os.urandom(2), 'little')
    else:
        return current_ids[0] + 1


class Trial:
    @staticmethod
    def get(id):
        try:
            with open(f'/{_TRIAL_DIR}/{id}') as fp:
                data = ujson.load(fp)
        except OSError:
            logging.error(f'Trial not found [{id=}]')
        else:
            return Trial(**data)
    
    @staticmethod
    def list():
        return [int(entry[0]) for entry in os.ilistdir(f'/{_TRIAL_DIR}')]

    @staticmethod
    def delete(id):
        try:
            os.remove(f'/{_TRIAL_DIR}/{id}')
            logging.info(f'Trial deleted [{id=}]')
        except OSError:
            logging.warn(f'Trial deletion failed, unknown id [{id=}]')
    
    def __init__(self, timestamp, capacitor, data, id = None):
        if id is None:
            self.id = _create_id()
        else:
            self.id = id
        self.timestamp = timestamp
        self.capacitor = capacitor
        self.data = data
    
    def as_dict(self):
        return {
            'id': self.id,
            'timestamp': self.timestamp,
            'capacitor': self.capacitor,
            'data': self.data
        }
    
    def save(self):
        data = self.as_dict()
        with open(f'/{_TRIAL_DIR}/{self.id}', 'w') as fp:
            ujson.dump(data, fp)
        logging.info(f'Trial saved [{self.id=}]')