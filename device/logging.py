import time
from micropython import const


DEBUG = const(0)
INFO = const(1)
WARNING = const(2)
ERROR = const(3)
CRITICAL = const(4)


log_level = WARNING
_log = []


def _write_log(level, msg):
    if level >= log_level:
        timestamp = time.time_ns()
        # # Do not convert to float, single is unsuitable for precise timestamps
        # now_seconds = now // 1000
        # now_millis = now % 1000
        # timestamp = f'{now_seconds}.{now_millis}'
        _log.append((timestamp, level, msg))


def debug(msg):
    _write_log(DEBUG, msg)


def info(msg):
    _write_log(INFO, msg)


def warn(msg):
    _write_log(WARNING, msg)


def error(msg):
    _write_log(ERROR, msg)


def critical(msg):
    _write_log(CRITICAL, msg)


def get_log():
    return _log


def get_log_size():
    return len(_log)


def clear_log():
    global _log
    del _log
    _log = []


def dump_log(name='sys.log'):
    with open(name, 'a') as fp:
        for entry in _log:
            fp.write('$'.join(entry))
            fp.write('\n')
