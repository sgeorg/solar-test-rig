import os
import logging
from comm import register_handler, send


@register_handler(1)
def handle_get_state(msg):
    # TODO: get state
    send({'state': 1})


@register_handler(2)
def handle_get_trial_list(msg):
    # TODO: get trial list
    data = {'trials': [1, 2, 3]}
    send(data)


@register_handler(3)
def handle_get_trial(msg):
    trial_id = msg['id']
    # TODO: get trial
    send({'data': None})


@register_handler(4)
def handle_get_trial_metadata(msg):
    trial_id = msg['id']
    # TODO: get trial metadata
    send({'data': None})


@register_handler(5)
def handle_delete_trial(msg):
    trial_id = msg['id']
    # TODO: delete trial
    send(True)


@register_handler(6)
def handle_get_log(msg):
    send({'log': logging.get_log()})


@register_handler(99)
def handle_restore_repl(msg):
    os.dupterm(port, 1)  # enable REPL
    logging.debug('REPL enabled')