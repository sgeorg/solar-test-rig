# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
#uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
#import webrepl
#webrepl.start()
gc.collect()


import logging
import comm


logging.log_level = logging.DEBUG

logging.info('Device started')

# Setup handler functions
import comm_handlers

comm.init()


# Start main loop
logging.info('Entering main loop')
while True:
    comm.handle_msg()
