#!/usr/bin/env python3
from typing import Any, Dict, List, Literal
#import msgpack
import json
import serial
import sys


port: serial.Serial = None  # type: ignore


def _send(data: Dict[str, Any]) -> None:
    preampel = b'\x12\x34\x56'
#    body = msgpack.dumps(data)  # type: bytes
    body = json.dumps(data).encode('utf-8')
    body_size = len(body)
    message = preampel + body_size.to_bytes(2, 'little') + body
    port.write(message)


def _recv() -> Dict[str, Any]:
    for i in [0x12, 0x34, 0x56]:
        if port.read(1)[0] != i:
            raise Exception('synchronization error')
    body_size = int.from_bytes(port.read(2), 'little')
    body = port.read(body_size)
#    return msgpack.loads(body)  # type: ignore
    return json.loads(body)


def init(portname: str) -> None:
    global port
    port = serial.Serial(portname, 115200)


def get_state() -> str:
    _send({'cmd': 1})
    while not port.in_waiting:  # Wait for response
        pass
    response = _recv()
    if 'error' in response:
        raise Exception(response['error'])
    state_id = response['state']
    return {
        1: 'idle',
        2: 'trial',
        3: 'wait'
    }[state_id]


def get_trial_list() -> List[int]:
    _send({'cmd': 2})
    while not port.in_waiting:  # Wait for response
        pass
    response = _recv()
    if 'error' in response:
        raise Exception(response['error'])
    return response['trials']


def get_trial(id: int) -> Dict[str, Any]:
    data = {
        'cmd': 3,
        'id': id
    }
    _send(data)
    while not port.in_waiting:  # Wait for response
        pass
    response = _recv()
    if 'error' in response:
        raise Exception(response['error'])
    return response


def get_trial_metadata(id: int) -> Dict[str, Any]:
    data = {
        'cmd': 4,
        'id': id
    }
    _send(data)
    while not port.in_waiting:  # Wait for response
        pass
    response = _recv()
    if 'error' in response:
        raise Exception(response['error'])
    return response


def delete_trial(id: int) -> None:
    data = {
        'cmd': 5,
        'id': id
    }
    _send(data)
    while not port.in_waiting:  # Wait for response
        pass
    response = _recv()
    if 'error' in response:
        raise Exception(response['error'])


def main():
    port, command = sys.argv[1:3]
    init(port)
    if command == 'log':
        _send({'cmd': 6})
        log = _recv()['log']
        for entry in log:
            print(f'{entry[0]/1e9}\t{entry[1]}\t{entry[2]}')
    elif command == 'list':
        for id in get_trial_list():
            print(id)
    elif command == 'trial':
        id = sys.argv[3]
        trial = get_trial(id)
        print(trial)
    elif command == 'delete':
        id = sys.argv[3]
        ...
    elif command == 'repl':
        _send({'cmd': 99})
    elif command == 'state':
        print(get_state())


if __name__ == '__main__':
    main()