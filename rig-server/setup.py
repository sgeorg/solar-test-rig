#!/usr/bin/env python3


from setuptools import setup



setup(
    name='rigserver',
    packages=['rigserver'],
    install_requires=[
        'flask',
        'flask-login',
        'peewee',
    ]
)
