#!/usr/bin/env python3

from datetime import datetime
import hashlib
from rigserver import app
from flask import render_template, redirect, url_for, request
from flask_login import login_required, current_user, login_user, logout_user
from rigserver.models import Trials, Users


@app.route('/')
def index():
    print(current_user)
    if current_user.is_anonymous:  # type: ignore
        return redirect(url_for('login'))
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    login_name = request.form.get('login_name', '')  # TODO sanity check
    password = request.form.get('password', '')  # TODO sanity check
    user = Users.get(login_name=login_name)  # TODO handle user not found
    hash = hashlib.new('sha3_384')
    hash.update(user.password_salt.encode('utf-8') + password.encode('utf-8'))
    if hash.hexdigest() == user.password_hash:
        login_user(user)
        return redirect(url_for('index'))
    else:
        # TODO flash error message
        return redirect(url_for('login'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/list', methods=['GET', 'POST'])
@login_required
def list_trials():
    # TODO search and filter results
    # TODO if admin then show all trials
    trials = (Trials.select()
                    .join(Users)
                    .where(Trials.user==current_user)  # type: ignore
                    .order_by(Trials.date.desc()))
    return render_template('list_trials.html', trials=trials)


@app.route('/show/<int:trial_id>')
@login_required
def show_trial(trial_id):
    trial = Trials.get(trial_id)  # TODO handle trial not found
    # TODO generate image url
    return render_template('show_trial.html', trial=trial)


@app.route('/new', methods=['GET', 'POST'])
@login_required
def new_trial():
    if request.method == 'GET':
        return render_template('new_trial.html')
    short_description = request.form.get('short_description', '<Trial>')
    long_description = request.form.get('long_description', '')
    duration = float(request.form.get('duration', '0'))
    ramp_up_time = float(request.form.get('ramp_up_time', '0'))
    trial = Trials.create(
        short_description=short_description,
        long_description=long_description,
        date=datetime.now(),
        user=current_user,
        duration=duration,
        ramp_up_time=ramp_up_time
    )
    trial.save()
    # TODO start trial
    return redirect(url_for('index'))  # TODO redirect to list_trials


# TODO image generation and download


# TODO report generation and download


# TODO data download


@app.route('/settings')
@login_required
def settings():
    ...


@app.route('/users/list')
@login_required
def users_list():
    ...


@app.route('/users/new', methods=['GET', 'POST'])
@login_required
def users_new():
    ...


@app.route('/users/delete/<int:user_id>')
@login_required
def users_delete(user_id):
    ...


# TODO modify user