# Source: https://www.youtube.com/watch?v=oNalXg67XEE


subscribers = {}


def subscribe(event: str, fn):
    if event not in subscribers:
        subscribers[event] = []
    subscribers[event].append(fn)


def post_event(event: str, data):
    if event not in subscribers:
        return
    for fn in subscribers[event]:
        fn(data)