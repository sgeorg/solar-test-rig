import os
from flask_login import UserMixin
from peewee import DateTimeField, FloatField, ForeignKeyField, Model, TextField, BooleanField, SqliteDatabase


db = SqliteDatabase(os.getenv('DB_FILE'))
db.connect()  # TODO possibly rename?


class ModelBase(Model):
    class Meta:
        database = db


class Users(ModelBase, UserMixin):
    login_name = TextField(unique=True)
    full_name = TextField()
    password_salt = TextField()  # TODO replace with BlobField
    password_hash = TextField()
    is_logged_in = BooleanField(default=False)
    is_admin = BooleanField(default=False)


class Trials(ModelBase):
    short_description = TextField()
    long_description = TextField()
    date = DateTimeField()
    user = ForeignKeyField(Users)
    duration = FloatField()
    ramp_up_time = FloatField()
    u_max = FloatField(default=0.0)
    i_max = FloatField(default=0.0)
    # TODO add field for actual meassurements
    # TODO add field for currently active  measurement
    # TODO add field for selected capacitor
