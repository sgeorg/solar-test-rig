import os
from flask import Flask
from flask_login import LoginManager, login_manager
from .models import Users


app = Flask(__name__)
app.secret_key = os.getenv('FLASK_SECRET')

login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def user_loader(id):
    id = int(id)
    return Users.get(id)


import rigserver.views
