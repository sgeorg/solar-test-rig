"""
Use this interactive script to setup the database after installing rigserver
for the first time.

To run the script you must activate the python environment in which rigserver
is installed. Then run

python setup-db.py

Then follow the setup process.
"""

import hashlib
import os
from getpass import getpass
from pathlib import Path


path = Path('/var/lib/rigserver/data.db')
new_path = Path(input('Path to database [{}]: '.format(path)))  # FIXME Path('') -> True
if new_path:
    path = Path(new_path).resolve()
path.parent.mkdir(parents=True)
os.environ['DB_FILE'] = str(path)

# Don't move the import. This will fail, if DB_FILE is not set before import.
from rigserver.models import db, Users, Trials
db.create_tables([Users, Trials])

print('Database created at {}.'.format(path))
print('Enter information for initial admin account.')

login_name = input('Login name: ')
full_name = input('Full name: ')
password = getpass()
password_check = getpass('Retype Password: ')
if password != password_check:
    print('Passwords are not the same. Abort.')
    exit()

salt = os.urandom(6).hex()
hash = hashlib.new('sha3_384')
hash.update(salt.encode('utf-8') + password.encode('utf-8'))

user = Users.create(
    login_name=login_name,
    full_name=full_name,
    password_salt=salt,
    password_hash=hash.hexdigest(),
    is_admin=True
)
user.save()

print('User {} created.'.format(login_name))
print('Setup done.')
