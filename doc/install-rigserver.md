# Install Rig Server

This document will guide you through the installation process for *rig server*

## Preconditions

* Python 3.7 or above
* knowledge of the command line of your system, e.g. bash, CMD, ...

While it should not be strictly nessessary, *rig server* was developed and
tested on Linux an we recommend to use a Linux based operating system to run
it. The following install guide will assume that you are using Linux.

## Install Guide

1. Get the latest rigserver.whl file from this repository
2. Create a virtual python environment `virtualenv --python=python3 venv` and
activate it `source venv/bin/activate`
3. Install the rig server `pip install rigserver-xy.whl`
4. From the tools folder run the script `setup-db.py` and follow its
instructions. It will create the database file and guide you through the
creation of the first (admin) user

The server is now set up an can be started.

## Starting

The server can be run with a single command, but some settings need to be done
before. These settings must be saved as environment variables, to be available
to the server. The settings are:

* `DB_FILE` the location of the sqlite database, as set with the setup-db.py script
* `FLASK_SECRET` the key with which client side cookies are signed. Use a long, randomly generated sequence e.g. `os.urandom(16).hex()`
* `FLASK_APP` must be `rigserver`
* `FLASK_ENV` should be `production` unless you know what you are doing

In addition to the settings, the virtual environment must be activated (see above).
When all is done, the server can be started with the command

`flask run --host <host> --port <port>`

where `<host>` and `<port>` are the host and port of the server respectively.

A complete start script might look like this

```
#!/usr/bin/env bash
source venv/bin/activate
export DB_FILE=/var/lib/rigserver/data.db
export FLASK_SECRET=sup3rs3cr3tk3y
export FLASK_APP=rigserver
export FLASK_ENV=production
flask run --host=0.0.0.0