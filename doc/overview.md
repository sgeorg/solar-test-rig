# Test Rig Overview

This project covers the hardware an software for a complete test rig for solar
cells. This means not only the electrical components but also a controll
interface, data visualization and data management. This document gives a quick
overview of the whole project and what the different folders in this repository
contain.

## Main Components

The solar test rig can be divided into the following main components:

* **Test Rig** is the actual electronics, microcontroller and interface that
does the measuring. This can be further broken down into:
    * **Measuring electronics** schematics TODO.
    * **User Interface** is a physical interface for the operator. schematics TODO.
    * **Microcontroller** see below.
* **Rig Server** manages data and operators.

## Microcontroller

TODO

## Rig Server

TODO

## Example Measurement Processes

1. The operator creates a new measurement on the *rig server* and enters
metadata
2. The *rig server* saves the new measurement and makes an interface available
for uploading data
3. The operator starts the measurement process on the *test rig*
4. The *microcontroller* checks for open measurements on the *rig server*
5. If there is an open measurement, the *microcontroller* saves the data to the
*rig server*
6. The operator finalizes the measurement, which closes it for further data