# Device Protocol

**DEPRECATED: The information in this document is outdated!**

Protocol for controlling the actual measuring device.

## Introduction

The control protocol is REST based. The micro-controller opens an HTTP server
on port 80, which listens to commands and requests.

## Endpoints

**/config/ramp_up_time**

Get (GET) or set (POST) the ramp up time for the meassurement. Message body is
the time in seconds as a JSON value. Only usable in state *idle*.

Returns:

* **200 OK** Return the current ramp up time/Configuration Successful
* **400 Bad Request** Invalid ramp up time
* **409 Conflict** The device is not in a state were the configuration can be changed

**/config/duration**

Get (GET) or set (POST) the duration for the meassurement. Message body is the
time in seconds as a JSON value. Only usable in state *idle*.

Returns:

* **200 OK** Return the current duration/Configuration Successful
* **400 Bad Request** Invalid duration
* **409 Conflict** The device is not in a state were the configuration can be changed

**/state**

Get (GET) the current state of the measuring device. The state is returned in
the message body of a *200 OK* message and can be one of the following values:

* **idle** The device is ready to accept commands
* **measuring** The device is currently performing a trial
* **reset** The device is reseting the testing rig after a trial

**/start**

Start (POST) a trial with the current configuration. The body of the POST
message is ignored and should be empty. If the trial can be started, the
response will contain a *trial id* in the message body with which the data can
be requested. Only usable in state *idle*.

Returns:
* **200 OK** The trail will start immediately
* **409 Conflict** The device is not in a state were a trial can be started

**/data/<trial id>**

Get (GET) the complete data set for the trial *trial id*, if available. This
command can be used during *idle*, *measuring* and *reset* states. The data
returned are JSON documents.

Returns:

* **200 OK** Returns the trial data
* **404 Not Found** Not data for *trial id* available